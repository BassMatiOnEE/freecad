﻿<!DOCTYPE html>
<html lang="en-US"><head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="template-info" file-name="empty.htm" version="2022-02-09" editor="USP" />
<meta name="author" content="USP" />
<meta name="creation-date" content="2022-03-30" />
<meta name="editor" content="USP" />
<meta name="change-date" content="2022-03-30" />
<meta name="version" content="1" />
<meta name="categories" content="" />
<meta name="tags" content="" />
<meta name="description" content="The sketcher produces 2D geometries on a sketch plane for use in other workbenches." />

<link rel="stylesheet" href="/inc/page.css" />
 
<title>FreeCAD Sketcher Workbench</title>

</head><body><header id="page-header"></header><div id="main-toolbar"></div><main id="page-content">

<h1>Sketcher Workbench</h1>

<p id="page-abstract"></p>

<p>A sketch consist of <em>sketch elements</em>, such as points, lines, arcs, circles, et cetera, and <em>constraints</em> (dimensions or relations to other sketch elements). The sketch geometry is then used to create elements in 3D space, <em>vertices</em> and (straight or curved) <em>edges</em>. </p>

<p>A sketch plane is always flat. Drawing on a warped plane is not possible. The sketch plane is <em>attached</em> to existing geometry elements with optional offsets and rotations. As a result, a sketch plane can have any possible position and orientation in 3D space.</p>

<p>A completed sketch is then used to create 3D features, such as pads, pockets or revolutions in the <tt>Part Design Workbench</tt>, or can be used as basis for other sketches and features ("a skeleton sketch").</p>

<h2 cbc>Creating a New Sketch</h2><div>

<p>An example to get you warmed up:</p>

<ol class="collapsible instructions">
<li>Click the menu <tt>View > Panels</tt> and make sure the <tt>Combo View</tt> item is checked.</li>
<li>Click the menu <tt>View > Workbench > Sketcher</tt>.</li>
<li>Click the document node in the <tt>Model</tt> pane in the <tt>Combo View</tt>. This makes sure that there is nothing selected in the model, otherwise the orientation dialog might not show up.</li>
<li>Click <tt>Sketch > Create sketch</tt>.</li>
<li>Choose the appropriate orientation, direction and offset, then click <tt>OK</tt>
	<br/><img src="./img/fig-001.png" width="185" style="margin-top:10px"/></li>
</ol>

<p>This gets you to the sketcher task panel, where you can manage the drawing elements and constraints that are used to define the sketch:</p>

<div class=""><img src="./img/fig-002.png" width="410" /></div>

<p>The task panel has four collapsible sub-panels named <tt>Solver messages</tt>, <tt>Edit controls</tt>, <tt>Constraints</tt> and <tt>Elements</tt>. </p>

<p>Click the menu <tt>Sketch > Leave</tt> to close the sketch editor, or the <tt>Close</tt> button on the <tt>Task</tt> panel.</p>

<p>If you have a already have geometry elements (a face, a line, points) selected when you create a new sketch, the sketch plane will be automatically be attached to them. Therefore the attachment editor is skipped and you are taken directly to the sketcher workspace.</p>

<p>Before adding <a href="#elements">geometry elements</a>, we will have a look at the attachment dialog.</p>

<!--h2--></div>

<h2 cbc>Attachment Dialog</h2><div>

<p>If you create a new sketch in the <tt>Part Design Workbench</tt>, the <tt>Sketch Orientation Dialog</tt> does not appear. Instead, you are taken to the <tt>Attachment Task</tt>:</p>

<ol class="collapsible instructions">
<li>Click <tt>View > Workbench > Part Design</tt>.</li>
<li>Activate the <tt>Model</tt> tab.</li>
<li>Click <tt>Part Design > Create Body</tt>.</li>
<li>Click <tt>Sketch > Create sketch</tt>. This opens the <tt>Sketch Attachment</tt> task:</li>
</ol>

<div><img src="./img/fig-003.png" width="392" /></div>

<p>The purpose is to determine the position and orientation of the sketch plane in 3D space. The 3D window shows the <tt>Origin</tt> planes of the current <tt>Body</tt> along with the current model (currently there is no model, just the body origin planes). You can select</p>

<ul>
<li>one of the body origin planes,</li>
<li>any other plane of the model,</li>
<li>up to four geometry elements for more complicated attachements,</li>
<li>or nothing at all and click <tt>OK</tt>.</li>
</ul>

<p>If you don't select anything and just click <tt>OK</tt>, you will be taken directly to the <tt>Sketcher Task</tt>. The sketch will be detached and oriented  parallel to the XY plane of its container element. (If there is no container, the sketch will be oriented parallel to world XY plane.)</p>

<ol class="collapsible instructions continue">
<li>Click the XY (or any other) plane. The selected plane's name appears in the <tt>Reference 1</tt> field. The task panel status (above) should change from black to green and says "Attached with mode Plane face".</li>
</ol>

<div><img src="./img/fig-004.png" width="392" /></div>

<ol class="collapsible instructions continue">
<li>Click <tt>OK</tt> to continue.</li>
</ol>

<p>The result appears in the <tt>Attachment</tt> section on the <tt>Property Panel</tt>:</p>

<div style="overflow-x:scroll"><img src="./img/fig-005.png" /></div>

<p>Specifically, in the <tt>Support</tt>, <tt>Map Mode</tt> and <tt>Map Reversed</tt> properties. Additional parameters appear under the <tt>Attachment Offset</tt> node. A click on the button in the right corner of the <tt>Support</tt> value field invokes an object selector dialog. Here you can exchange one or more objects to attach the sketch plane to.</p>

<p>A more complete option for reattachment provides the button in the <tt>Map Mode</tt> value field, it re-invokes the attachment editor, with all the options already described above. By the way, you are not limited to planar faces, you can also attach to perpendicular to a line, or to a plane defined by 3 points in space, et cetera.</p>

<!--h2--></div>

<h2 cbc id="elements">Adding Geometry Elements</h2><div>

<p>The available geometry elements can be found in the <tt>Sketch > Create geometries</tt> menu:</p>

<div><img src="./img/fig-006.png" /></div>

<p>We will not go through all of them, most of them are self-explaining. Additional documentation can be found in the <a href="https://wiki.freecadweb.org/Sketcher_Workbench#Sketcher_geometries">FreeCAD Wiki</a>. For a starter, create the following lines with the <tt>Sketch > Geometries > Line</tt> tool:</p>

<div><img src="./img/fig-007.png" /></div>

<p>Click to set the first point, click again to set the end point. Drag the line to the desired position, or drag an end point to resize the line. You will notice that you cannot enter coordinates. This is fine for now, in the next section you will learn to add <em>constraints</em> to define dimensions, angles, et cetera.</p>

<p>The drawing elements appear in the elements listbox:</p>

<div><img src="./img/fig-008.png" /></div>

<p>If you have something like above, you are ready to proceed to the next section.</p>

<!--h2--></div>

<h2 cbc>Adding Constraints</h2><div>

<p>In this section we will add <em>constraints</em> to define the sketch. Constraints are dimensions or relations between sketch elements. To begin we add <em>coincident constraints</em> to the end-points of lines:</p>

<ol class="collapsible instructions">
<li>Click the two topmost line endpoints to select them.</li>
<li>Click the menu <tt>Sketch > Constraints > Coincident</tt>.</li>
</ol>

<p>The two endpoints are now connected. Try that by moving the lines a little bit around.</p>

<p>The constraint appears in the <tt>Constraints</tt> listbox as "Constraint1". You can change the name if you like, but this is generally not done for simple constraints.</p>

<p>Another method is to activate the tool first, then select the points:</p>

<ol class="collapsible instructions continue">
<li>Click the menu <tt>Sketch > Constraints > Coincident</tt>. Alternatively, type the letter <tt>C</tt> on the keyboard. The mouse pointer changes according to the selected tool.</li>
<li>Click one of the leftmost end points.</li>
<li>Click the other left end point. Another coincident constraint was created.</li>
<li>Click the two remaining endpoints. The third coincident constraint was created.</li>
</ol>

<p>You should now have something like this:</p>

<div><img src="./img/fig-009.png" /></div>

<p>These constraints we have created so far:</p>

<div><img src="./img/fig-010.png" width="338"/></div>

<p>But we are not ready yet. Expand the <tt>Solver Messages</tt> panel:</p>

<div><img src="./img/fig-011.png" width="338"/></div>

<p>The sketch is still under-constrained, and we have 6 degrees of freedom. Click on the DoF link, and the underconstrained elements are highlighted in the 3D window. Line endpoints in this case. In crowded sketch, it might be difficult to find them. If so, scroll the <tt>Task Panel</tt> down to the <tt>Elements</tt> listbbox and uncheck <tt>Auto-Switch to Edge</tt>. (No, we are not defaulting to Chrome or Firefox here ;-) Then switch the <tt>Type</tt> in the dropdown-listbox above from <tt>Edge</tt> to <tt>Starting Point</tt> or <tt>End Point</tt>. The related line elements should now be highlighted in the elements listbox.</p>

<p>Let's provide additional constraints. But before, let's look at methods to select sketch elements.</p>

<!--h2--></div>

<h2 cbc>Selecting Sketch Elements</h2><div>

<p>Sketch elements may be stacked on top of each other, and it may be difficult to select a particular element, or determine which element is actually selected, or not. To make that clear, we can use the <tt>Selection View</tt> panel. Click the menu <tt>View > Panels</tt>, and make sure that the submenu <tt>Selection View</tt> is checked. I usually have it docked on a tab, but the panel can be pulled off to a floating window.</p>

<p>Click to select the left corner point of the triangle. This is vertex four:</p>

<div><img src="./img/fig-012.png" width="301"/></div>

<p>Another method is to click and drag a selection rectangle with the mouse down right. All elements that are entirely within the boundaries of the selection rectangle are selected. Try it on the left corner:</p>

<div><img src="./img/fig-013.png" width="300"/></div>

<p>We found the vertices 4 and 5. Obviously, vertex 4 hides vertex 5.</p>

<p>Another method is to drag a selection rectangle up left. All elements that are at least partially inside this rectange are selected. Try it on the left corner of the triangle in the 3D view:</p>

<div><img src="./img/fig-014.png" width="291"/></div>

<p>Now we caught two lines and two endpoints.</p>

<p>Every click on an element in the 3D view toggles its selection state. If you miss and click into the empty space, the selection list is cleared. This can be very frustrating sometimes. To prevent unintentional deselection, hold the control key down while clicking. When done, apply a tool (a constraint for instance) on the selected elements.</p>

<p>Another way to avoid unintended selection is to <em>select the tool first</em>, then click the desired elements to apply the tool.</p>

<p>Another method to select sketch elements is to use the <tt>Elements</tt> pane on the <tt>Tasks</tt> panel. Select an entry from the listbox, and the corresponding element should be highlighted in the 3D window. Play with <tt>Type</tt>, and switch between edge and points.</p>

<p>Example:</p>

<ol class="collapsible instructions">
<li>Select the menu <tt>Sketch > Geometries > Circle</tt>.</li>
<li>Click in the 3D window to set the center point.</li>
<li>Click a second time to define the radius.</li>
<li>Repeat to create two more circles. Set the centerpoints slightly offset vertically.</li>
<li>On the <tt>Elements</tt> panel, enable <tt>Auto-switch to Edge</tt> (which is the default).</li>
</ol>

<div><img src="./img/fig-015.png" width="220"/></div>

<p>Now we want make the circle centers horizontal.</p>

<ol class="collapsible instructions continue">
<li>Click to select the entry <tt>4-Circle</tt> on the <tt>Elements</tt> pane.</li>
<li>Shift-click the <tt>6-Circle</tt>.</li>
</ol>

<p>Now all three circles are selected, and we are going to apply the horizontal constraint to them:</p>

<ol class="collapsible instructions continue">
<li>Click the menu <tt>Sketch > Constraints > Horizontal</tt>.</li>
</ol>

<p>We get an error message: "The selected edge is not a line segment". Well, yes, of course, they are circles, not lines. After a while of thinking we get it: We must constrain the <em>center points</em> horizontally. To select them, tweak <tt>Elements</tt> panel:</p>

<ol class="collapsible instructions continue">
<li>Click on an empty space in the 3D window to deselect everything.</li>
<li>Turn off <tt>Auto-switch to Edge</tt>.</li>
<li>Select <tt>Center Point</tt> into the <tt>Type</tt> combo box.</li>
<li>Select the three circle elements in the list box.</li>
</ol>

<p>Now the three center points are selected, and we can apply our constraint:</p>

<ol class="collapsible instructions continue">
<li>Click the menu <tt>Sketch > Constraints > Horizontal</tt>.</li>
</ol>

<p>There we got it:</p>

<div><img src="./img/fig-016.png" width="224" /></div>

<p>The constraints also appear in the <tt>Constraints</tt> listbox:</p>

<div><img src="./img/fig-017.png" width="339" /></div>

<p>If we select them, they are highlighted in the 3D window.</p>

<p class="note">NOTE that element selection in list boxes should follow the operating system standards. That means that a control-click toggles the selection state of an entry, and shift-click selects all entries between the entry that previously had the focus (indicated by a dotted rectangle) and the entry that received the click. Additionally, arrow keys and spacebar in combination with shift and control should be usable to select entries. However, the developers can easily override the standard behavior (and apparently have done so).</p>

<p>Now back to the three lines forming the triangle. We are going to select the three circles and delete them. This can be done in the 3D window (which would be the obviously easiest way), but we will do it with the <tt>Elements</tt> list box:</p>

<ol class="collapsible instructions continue">
<li>Switch elements <tt>Type</tt> to <tt>Edge</tt>.</li>
<li>Select the three circle elements in the list.</li>
<li>Press the <tt>Delete</tt> key or click the menu <tt>Edit > Delete</tt>.</li>
</ol>


<!--h2--></div>

<h2 cbc>Adding Construction Geometry</h2><div>
<p>In this section we will make the triangle equilateral in the most complicated way. For this purpose we will use <em>construction geometry</em>. Construction geometry is invisible outside the sketch editor, such elements are perfect construction helpers.</p>

<ol class="collapsible instructions">
<li>Click <tt>Sketch > Geometries > Toggle construction geometry</tt>. The geometry icons in the toolbar and in the sketcher geometries menu turn blue.</li>
<li>Place three lines on the sketch, like so:</li>
</ol>

<div><img src="./img/fig-018.png" width="341" /></div>

<p>Avoid creating automatic constraints for now by placing points at a distance from existing elements. Note that the lines are blue, indicating construction geometry elements.</p>

<ol class="collapsible instructions continue">
<li>Select the three points in the center of the triangle.</li>
<li>Click the menu <tt>Sketch > Constraints > Coincident</tt>. The line end points are pulled together.</li>
<li>Click the menu <tt>Sketch > Constraints > symmetrical</tt>. The mouse pointer changes to the symmetry tool.</li>
<li>Click two end points of the triangle, then click the outer end point of the related construction line. A symmetrical constraint has been created.</li>
<li>Repeat the previous step for the other sides.</li>
</ol>

<div><img src="./img/fig-019.png" width="335" /></div>

<p>Note that the symmetrical constraints are clearly visible in the 3D window, while coincident constraints are somehow hidden behind vertices. In fact, if you click on a vertex and press <tt>Delete</tt>, the <em>constraint</em> is deleted, not the vertex. (The point element might be an exception.)</p>

<p>Next, we create perpendicular constraints between triangle sides and the associated construction line.</p>

<ol class="collapsible instructions continue">
<li>Click the menu <tt>Sketch > Constraints > perpendicular</tt>.</li>
<li>Click a blue construction line.</li>
<li>Click the related triangle side.</li>
<li>Repeat the last two steps for the other two side and construction lines.</li>
</ol>

<p>The next step creates equality constraints between the blue construction lines.</p>

<ol class="collapsible instructions continue">
<li>Select the three construction lines in the <tt>Elements</tt> list box.</li>
<li>Select the menu <tt>Sketch > Constraints > equal</tt>.</li>
</ol>

<p>Now they have the same length. This also makes the triangle sides equal in length.</p>

<!--h2--></div>

<h2 cbc>A Fully Constrained Sketch</h2><div>

<p>Expand the <tt>Solver Messages</tt> panel. There are still four degrees of freedom. In this section we will reduce them to zero.</p>

<ol class="collapsible instructions">
<li>Click the menu <tt>Sketch > Tools > Select origin</tt>.</li>
<li>Click the intersection point of construction lines in the middle of the triangle.</li>
<li>Click the the menu <tt>Sketch > Constraints > Coincident</tt>.</li>
</ol>

<p>The position or the triangle is now fixed, we can only rotate and resize it.</p>

<ol class="collapsible instructions continue">
<li>Click to select the upper corner of the triangle.</li>
<li>Select the Y axis.</li>
<li>Click the menu <tt>Sketch > Constraints > point onto object</tt>.</li>
</ol>

<p>One degree of freedom left.</p>

<ol class="collapsible instructions continue">
<li>Select a triangle edge.</li>
<li>Click the menu <tt>Sketch > Contraints > Distance</tt>. A dialog box appears.</li>
<li>Enter 35 mm for the length.</li>
<li>Optional: Enter the name <tt>L1</tt> for the dimension name.</li>
<li>Click <tt>OK</tt>.</li>
</ol>

<p>The sketch is fully constraint and turns green.</p>

<div><img src="./img/fig-020.png" width="368" /></div>

<p>No degrees of freedom. That's what we want.</p>

<ol class="collapsible instructions continue">
<li>Click the menu <tt>Sketch > Leave</tt> to exit the sketch editor.</li>
</ol>

<p>To edit the sketch, double-click it in the model tree, or right-click on it and select <tt>Edit sketch</tt>.</p>

<!--h2--></div>

<h2 cbc>The Virtual Space</h2><div>

<p>There are times when sketches become overcrowded with all sorts of constraints. Fortunately, we have another "layer" we can switch to: The virtual space. This "space" shows the geometry elements as usual, but hides the constraints. To switch between these layers, use the menu command <tt>Sketch > Virtual Space > Switch</tt>. The constraints are still highlighted in the 3D windows when hover the mouse pointer over an entry in the <tt>Constraints</tt> listbox.</p>

<p>Note the checkboxes if front of the constraints in the <tt>Constraints</tt> listbox. They are now unchecked, which means they are not visible is the current "space". Switching the "virtual space" simply toggles their state. Setting a checkmark manually toggles their visibility in the current space.</p>

<div><img src="./img/fig-021.png" width="328" /></div>

<!--h2--></div>

<h2 cbc>Constraint Names</h2><div>

<p>As shown above, so-called <em>datum constraints</em> are frequently given <em>names</em>. This not makes sketch tweaking easier, it also gives dimensions a meaning, and facilitates the use of dimensions in formulas. Naming other constraints is possible, but I did not find that necessary.</p>

<ol class="collapsible constraints">
<li>Double-click the sketch in the model tree.</li>
<li>Expand the <tt>Constraint</tt> panel.</li>
<li>Select <tt>Datums</tt> into the <tt>Filter</tt> combo box.</li>
</ol>

<p>Only numerical constraints are shown, the others are hidden. Right-click on the distance constraint and explore the available options. For instance, rename.</p>

<ol class="collapsible constraints continue">
<li>Right click an entry, then click <tt>Rename</tt>.</li>
<li>Enter an appropriate name.</li>
</ol>

<p>I found that L followed by a number is appropriate for any length constraint. An R for radius, D for diameter, and A for angle is sufficient for me, but you can also choose more descriptive names if you like.</p>


<ol class="collapsible constraints continue">
<li>Leave the sketch.</li>
<li>On the <tt>Property Panel</tt>, in the <tt>Sketch</tt> section, expand the <tt>Constraints</tt> property.</li>
<li>Note that there are the named constraints.</li>
<li>Change the value to something different.</li>
</ol>

<p>As you can see, named datum constraints can be very handy.</p>


<div><img src="./img/fig-022.png" width="309" /></div>

<!--h2--></div>

<h2 cbc>Advanced Operations</h2><div>

<h3 cbc>Clone and Copy Geometry</h3><div>

<p>To copy sketch elements within the same sketch, select one or more sketch elements, then use the <tt>Sketch > Tools > Copy</tt> command. <tt>Ctrl-C</tt> is the equivalent keyboard shortcut. Move the mouse to the desired location. A rubberband line gives some visual feedback.</p>

<p>The <tt>Sketch > Tools > Clone</tt> command works pretty much in the same way, but if an element has a datum constraint on it, an equal constraint is created between the original and the copy.</p>

<!--h3--></div>

<h3 cbc>Carbon Copy</h3><div>

<p>The command <tt>Sketcher > Geometries > Carbon Copy</tt> creates a copy of another sketch in the current sketch in edit mode. The copied elements are detached from the source, so when you edit the first sketch, the second sketch will not be updated. All elements of the source sketch are copied, but you can delete unwanded elements without affecting the source sketch.</p>

<p>Datum constraints are also copied from the source sketch, but values are transformed to expressions that reference the source sketch constraint(s). If you change a datum constraint in the source sketch, the copy will be updated accordingly. Of course, you can detach the datum constraint from the source by replacing the expression with a literal value.</p>

<p>The carbon copy feature is useful if you have already a similar sketch, but need to make some changes (e. g. change hole diameters, remove geometry elements, et cetera). Note that carbon copy can be <em>repeated</em> if you have to create multiple sets of similar geometry.</p>

<p>If the source sketch is in a different body, press <tt>CTRL</tt>. The source and target sketch planes must be parallel. If not, press <tt>ALT-CTRL</tt> when selecting the source sketch. It may be necessary to close and re-open the sketch to make the carbon copy visible.</p>

<p>You may also consider the <a href="https://wiki.freecadweb.org/PartDesign_ShapeBinder">Part Design Shape Binder</a> or the <a href="https://wiki.freecadweb.org/PartDesign_SubShapeBinder">Sub-Objects Shape Binder</a>.</p>

<!--h3--></div>

<h3 cbc>External Geometry</h3><div>

<p>Often you will have to reference existing geometry, defined somewhere outside the sketch. If the geometry was defined in the same <tt>Body</tt>, then you can import that geometry and use it for contraints. The following example <a href="./example/inscribed-circle.fcstd">Inscribed Circle</a> demonstrates that.</p>

<ol class="collapsible instructions" >
<li>Create a new file.</li>
<li>Save it under the name "inscribed-circle.fcstd".</li>
<li>Switch to the <tt>Part Design Workbench</tt>.</li>
<li>Create a new <tt>Body</tt>. Note that the body is active.</li>
<li>Create a new <tt>Sketch</tt> and attach it to the XZ plane of the body's origin.</li>
<li>Create a rectangle.</li>
<li>Select two adjacent lines and make them equal.</li>
<li>Select two opposite corners and the sketch origin, and apply a <tt>Symmetric Constraint</tt>.</li>
<li>Apply a <tt>Distance Constraint</tt> on a side, set the length to 25 mm and name it "L1".</li>
<li>The sketch is now fully defined. Close it.</li>
<li>Rename it to <tt>Sketch000</tt>.</li>
</ol>

<p>Now we will create a second sketch on the same plane, with a Z offset of 25 mm.</p>

<ol class="collapsible instructions continue">
<li>Create a new <tt>Sketch</tt>.</li>
<li>Attach it to the XY plane.</li>
<li>Click the menu <tt>Sketch > Geometries > External</tt>.</li>
<li>Click one of the four sides of the square. A line element is added to the <tt>Elements</tt> list box.</li>
<li>Create a circle with the center on the sketch origin. Drag the radius near the external line. That should create a <tt>Tangent Constraint</tt>.</li>
<li>Verify that the sketch is now fully constrained.</li>
<li>Click the menu <tt>Sketch > Constraints > Toggle driving/reference</tt>. The constraints icons in the menu and on the toolbar should now appear in a blue color.</li>
<li>Create a <tt>Diameter Reference Constraint</tt> on the circle's circumference edge.</li>
<li>Right-click the diameter constraint in the <tt>Constraints</tt> list box, then click <tt>Rename</tt>. Enter the name <tt>D1</tt>.</li>
<li>Close the sketch editor.</li>
</ol>

<p>Now we will offset the z-plane a little bit.</p>

<ol class="collapsible instructions continue">
<li>Make sure that <tt>Sketch001</tt> is selected in the <tt>Model Tree</tt>.</li>
<li>Click the <tt>Map Mode</tt> on the <tt>Property Panel</tt>.</li>
<li>Click the ellipsis button in the value field to open the <tt>Attachment Editor Task</tt>.</li>
<li>Set the <tt>Attachment Offset in Z Direction</tt> to 25 mm.</li>
<li>Click OK to close the <tt>Task Panel</tt>.</li>
<li>Expand the <tt>Attachment</tt> property on the <tt>Property Panel</tt>.</li>
<li>Expand the <tt>Position</tt> property.</li>
<li>Verify that the z offset has been set correctly. If necessary, edit the value as needed.</li>
</ol>

<p>You should now have something like this on your screen:</p>

<div><img src="./img/fig-023.png" width="481" /></div>

<p>Note that the constraint <tt>L1</tt> in Sketch000 also drives the size of the circle in Sketch001:</p>

<ol class="collapsible instructions continue">
<li>Select <tt>Sketch000</tt> in the model tree.</li>
<li>Expand the <tt>Sketch > Constraints</tt> property, and change the value to <tt>35 mm</tt>.</li>
<li>Click <tt>Edit > Refresh</tt> or press <tt>F5</tt> to update the model. The size of square and circle should have changed.</li>
<li>Select <tt>Sketch001</tt> in the model tree.</li>
<li>Expand the <tt>Sketch > Constraints</tt> property.</li>
<li>Verify that <tt>D1</tt> is now 35 mm.</li>
</ol>

<p>D1 is a reference or driven constraint and cannot be changed directly. To verify that,&hellip;</p>

<ol class="collapsible instructions continue">
<li>Change the value of <tt>D1</tt> to 45 mm.</li>
<li>It appears as if the change succeeded. But&hellip;</li>
<li>Click <tt>Edit > Refresh</tt> or press <tt>F5</tt> to recompute the model.</li>
<li>Verify that the <tt>D1</tt> was reset to 35 mm.</li>
</ol>

<!--h2--></div>

<!--h2--></div>

<h2 cbc>Additional Information</h2><div>

<h3 cbc>Internal Alignment</h3><div>

<p>Internal aligment is used to constrain complex geometry elements, such as ellipse, hyperbola, parabola, or b-spline. It can be hidden from the <tt>Constraints</tt> listbox.</p>

<p>References: <a href="https://forum.freecadweb.org/viewtopic.php?f=8&t=55061">Sketcher Internal Alignment Geometry</a> by Abdullah on FreeCAD Wiki.</p>

<!--h3--></div>

<!--h2--></div>

<h2 cbc>Preferences</h2><div>

<p>The sketcher preferences are accessed from the menu <tt>Edit > Prefernces</tt>. Select the <tt>Sketcher</tt> icon in the left hand side. If not available, click the <tt>Workbenches</tt> icon and click the <tt>Sketcher</tt> entry from the list of loadable workbenches first.</p>

<p>The sketcher workbench has three preference tabs: <tt>General</tt>, <tt>Display</tt>, and <tt>Colors</tt>.</p>

<h3>Display</h3>

<dl>
<dt>Font Size</dt>
<dd>Determines the size of constraints in the the sketch.</dd>

<dt>View Scale Ratio</dt>
<dt>Determines the line thickness.</dt>
</dl>

<!--h2--></div>

</main><footer id="page-footer"></footer><script type="module" src="/inc/page.js" ></script></body></html>